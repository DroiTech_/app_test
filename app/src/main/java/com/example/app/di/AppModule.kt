package com.example.app.di

import android.content.Context
import androidx.room.Room
import com.example.app.data.AppConst.BASE_URL
import com.example.app.data.AppConst.DATABASE_NAME
import com.example.app.data.local.AppDatabase
import com.example.app.data.local.DbManager
import com.example.app.data.local.PostsDao
import com.example.app.data.remote.ApiManager
import com.example.app.data.repository.Repository
import com.example.app.data.remote.Endpoints
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRoomInstance(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        DATABASE_NAME
    ).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideCPostsDao(db: AppDatabase) = db.postsDao()

    @Singleton
    @Provides
    fun provideDbManager(postsDao: PostsDao): DbManager = DbManager(postsDao)

    @Singleton
    @Provides
    fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    @Singleton
    @Provides
    fun provideEndpoints(retrofit:Retrofit): Endpoints = retrofit.create(Endpoints::class.java)

    @Singleton
    @Provides
    fun provideApiManager(endpoints: Endpoints, @ApplicationContext context: Context): ApiManager = ApiManager(endpoints, context)

    @Singleton
    @Provides
    fun provideRepository(apiManager: ApiManager, dbManager: DbManager ): Repository = Repository(apiManager, dbManager)
}