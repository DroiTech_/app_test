package com.example.app.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.data.AppConst.IMAGE_BASE_URL
import com.example.app.data.models.Post
import com.example.app.data.repository.Repository
import com.example.app.data.repository.RepositoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    val imageLiveData = MutableLiveData<String?>()
    val detailsLiveData = MutableLiveData<RepositoryState<Post>>()

    fun handlePostData(postId: Int, userId: Int){
        prepareImageUrl(userId)
        getPostDetails(postId)
    }

    private fun getPostDetails(postId: Int){
        viewModelScope.launch {
            repository.getPostDetails(postId).collect{
                detailsLiveData.value = it
            }
        }
    }

    private fun prepareImageUrl(userId: Int){
        imageLiveData.value = IMAGE_BASE_URL.plus(userId)
    }
}