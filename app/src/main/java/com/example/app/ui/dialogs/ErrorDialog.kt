package com.example.app.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.app.R
import com.example.app.ui.details.DetailsFragmentArgs
import kotlinx.android.synthetic.main.error_dialog.*

class ErrorDialog : DialogFragment() {
    val args: ErrorDialogArgs by navArgs()
    private val viewModel by activityViewModels<ErrorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.error_dialog,
            container, false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dialog_button_cancel.setOnClickListener {
            findNavController().navigateUp()
        }
        dialog_button_retry.setOnClickListener{
            findNavController().navigateUp()
            viewModel.retry()
        }
        dialog_info.text = args.errorMsg

    }
}