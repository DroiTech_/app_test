package com.example.app.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app.R
import com.example.app.data.repository.RepositoryState
import com.example.app.ui.dialogs.ErrorViewModel
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.flow.collect


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel by activityViewModels<MainViewModel>()
    private val errorViewModel by activityViewModels<ErrorViewModel>()
    private lateinit var adapter: MainAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        setObservers()
        initPullToRefresh()
    }


    private fun initAdapter() {
        main_recycler_view.layoutManager = LinearLayoutManager(this.context)
        adapter = MainAdapter()
        main_recycler_view.adapter = adapter
        adapter.onItemClick = {
            val action =
                MainFragmentDirections.actionMainFragmentToDetailsFragment(it.userId, it.id)
            findNavController().navigate(action)
        }
    }

    private fun initPullToRefresh() {
        swipe_refresh.setOnRefreshListener {
            viewModel.getPosts()
        }
    }

    private fun setObservers() {
        viewModel.getPosts()
        viewModel.postsLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is RepositoryState.Loading -> {
                    swipe_refresh.isRefreshing = true
                    swipe_refresh.invalidate()
                    adapter.updateData(null)
                }
                is RepositoryState.Success -> {
                    swipe_refresh.isRefreshing = false
                    swipe_refresh.invalidate()
                    adapter.updateData(it.data)
                }
                is RepositoryState.Error -> {
                    swipe_refresh.isRefreshing = false
                    val action =
                        MainFragmentDirections.actionMainFragmentToErrorDialog(it.customError.msg)
                    findNavController().navigate(action)
                }
            }
        }
        errorViewModel.retryLiveData.observe(viewLifecycleOwner) {
            viewModel.getPosts()
        }
    }
}