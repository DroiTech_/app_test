package com.example.app.ui.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.app.R
import com.example.app.data.repository.RepositoryState
import com.example.app.ui.dialogs.ErrorViewModel
import com.example.app.ui.main.MainFragmentDirections
import kotlinx.android.synthetic.main.details_fragment.*
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.flow.collect

class DetailsFragment : Fragment() {

    val args: DetailsFragmentArgs by navArgs()

    companion object {
        fun newInstance() = DetailsFragment()
    }

    private val viewModel by activityViewModels<DetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.handlePostData(args.postId, args.userId)
        setObservers()
    }

    private fun setObservers() {
        viewModel.imageLiveData.observe(viewLifecycleOwner) {
            activity?.let { context ->
                Glide.with(context)
                    .load(it)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .circleCrop()
                    .into(imageView)
            }
        }
        viewModel.detailsLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is RepositoryState.Loading -> {
                }
                is RepositoryState.Success -> {
                    title.text = it.data?.title
                    description.text = it.data?.body
                }
                is RepositoryState.Error -> {
                }
            }
        }

    }

}