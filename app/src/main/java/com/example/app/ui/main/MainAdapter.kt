package com.example.app.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app.data.models.Post

class MainAdapter() : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    var onItemClick: ((Post) -> Unit)? = null
    val posts: ArrayList<Post> = mutableListOf<Post>() as ArrayList<Post>

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.title) as TextView
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(posts[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.main_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text =  posts[position].title
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    fun updateData(newPosts: List<Post>?) {
        posts.clear()
        notifyDataSetChanged()
        newPosts?.let { posts.addAll(it) }
        notifyDataSetChanged()
    }
}