package com.example.app.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.data.models.Post
import com.example.app.data.repository.Repository
import com.example.app.data.repository.RepositoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository): ViewModel() {
    val postsLiveData = MutableLiveData<RepositoryState<List<Post>>>()

    fun getPosts(){
        viewModelScope.launch {
            postsLiveData.value = RepositoryState.Loading
            repository.getPosts().collect{
                postsLiveData.value = it
            }
        }

    }
}

