package com.example.app.ui.dialogs

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class ErrorViewModel: ViewModel(){
    val retryLiveData = MutableLiveData<String>()

    fun retry() {
        retryLiveData.value = ""
    }
}