package com.example.app.data.repository

import com.example.app.data.local.DbManager
import com.example.app.data.models.Post
import com.example.app.data.models.asPostlEntity
import com.example.app.data.remote.ApiManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class Repository @Inject constructor(
    private val apiManager: ApiManager,
    private val dbManager: DbManager
) {
    suspend fun getPosts(): Flow<RepositoryState<List<Post>>> {
        return flow {
            emit(dbManager.getCachedPosts())
            when (val response = apiManager.getPosts()) {
                is RepositoryState.Success -> {
                    response.data?.let {
                        for (post in it) {
                            dbManager.savePost(post.asPostlEntity())
                        }
                    }
                }
                is RepositoryState.Error -> {
                    emit(response)
                }
                is RepositoryState.Loading -> {
                    emit(RepositoryState.Loading)
                }
            }
            emit(dbManager.getCachedPosts())
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPostDetails(postId: Int): Flow<RepositoryState<Post>> {
        return flow {
            emit(dbManager.getCachedPost(postId))
            when (val response = apiManager.getPostDetails(postId)) {
                is RepositoryState.Success -> {
                    response.data?.let {
                        dbManager.savePost(it.asPostlEntity())
                    }
                }
                is RepositoryState.Error -> {
                    emit(response)
                }
                is RepositoryState.Loading -> {
                    emit(RepositoryState.Loading)
                }
            }
            emit(dbManager.getCachedPost(postId))
        }.flowOn(Dispatchers.IO)
    }
}