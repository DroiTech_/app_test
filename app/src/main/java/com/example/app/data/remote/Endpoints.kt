package com.example.app.data.remote

import com.example.app.data.models.Post
import com.example.app.data.models.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Endpoints {
    @GET("posts")
    suspend fun getPosts(): Response<ArrayList<Post>>

    @GET("posts/{postId}")
    suspend fun getPostDetails(@Path("postId") postId: Int): Response<Post>

    @GET("users/{userId}")
    suspend fun getUser(@Path("userId") userId: Int): Response<User>
}