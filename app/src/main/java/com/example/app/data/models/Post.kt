package com.example.app.data.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
): Parcelable

@Entity(tableName = "postsTable")
data class PostEntity(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "body")
    val body: String = "",
    @ColumnInfo(name = "title")
    val title: String = "",
    @ColumnInfo(name = "userId")
    val userId: Int
)

fun Post.asPostlEntity(): PostEntity =
    PostEntity(this.id, this.body, this.title, this.userId)