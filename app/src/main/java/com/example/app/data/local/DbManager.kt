package com.example.app.data.local

import com.example.app.data.models.Post
import com.example.app.data.models.PostEntity
import com.example.app.data.repository.RepositoryState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class DbManager @Inject constructor(private val postsDao: PostsDao)  {
    suspend fun savePost(postEntity: PostEntity) {
        postsDao.savePost(postEntity)
    }

    suspend fun getCachedPosts(): RepositoryState<List<Post>> {
        return if(postsDao.getPosts()!=null) {
            RepositoryState.Success(postsDao.getPosts())
        }else{
            RepositoryState.Loading
        }
    }

    suspend fun getCachedPost(postId: Int): RepositoryState<Post> {
        return if(postsDao.getPost(postId)!=null) {
            RepositoryState.Success(postsDao.getPost(postId))
        }else{
            RepositoryState.Loading
        }
    }
}