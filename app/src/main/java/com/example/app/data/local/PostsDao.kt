package com.example.app.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.app.data.models.Post
import com.example.app.data.models.PostEntity

@Dao
interface PostsDao {
    @Query("SELECT * FROM postsTable")
    suspend fun getPosts(): List<Post>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePost(cocktail: PostEntity)

    @Query("SELECT * FROM postsTable WHERE id LIKE :postId ")
    suspend fun getPost(postId: Int): Post?
}