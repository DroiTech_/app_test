package com.example.app.data.repository

sealed class RepositoryState<out T: Any> {
    object Loading: RepositoryState<Nothing>()
    data class Success<out T : Any>(val data: T?): RepositoryState<T>()
    data class Error(val customError: CustomError) : RepositoryState<Nothing>()
}

data class CustomError(val msg: String, val code: Int, val ex: Exception?)