package com.example.app.data.models

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)