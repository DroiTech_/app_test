package com.example.app.data.remote

import android.content.Context
import android.net.ConnectivityManager
import com.example.app.R
import com.example.app.data.models.Post
import com.example.app.data.remote.ErrorCode.NO_INTERNET
import com.example.app.data.remote.ErrorCode.UNKNOWN_ERROR
import com.example.app.data.repository.CustomError
import com.example.app.data.repository.RepositoryState
import retrofit2.Response
import javax.inject.Inject

class ApiManager @Inject constructor(private val endpoints: Endpoints, private val context: Context) {

    suspend fun getPosts(): RepositoryState<List<Post>>{
        return safeApiCall( call = { endpoints.getPosts() })
    }

    suspend fun getPostDetails(postId: Int): RepositoryState<Post>{
        return safeApiCall( call = { endpoints.getPostDetails(postId) })
    }

    private suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): RepositoryState<T> {
        return try {
            if(!isNetworkAvailable()){
                return RepositoryState.Error(CustomError(context.getString(R.string.error_no_internet), NO_INTERNET, null))
            }
            val myResp = call.invoke()
            if (myResp.isSuccessful) {
                RepositoryState.Success(myResp.body())
            } else {
                RepositoryState.Error(CustomError(context.getString(R.string.error_general), myResp.code(), null))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            RepositoryState.Error(CustomError(context.getString(R.string.error_general), UNKNOWN_ERROR, e))
        }
    }

    @Suppress("DEPRECATION")
    private fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!
            .isConnected
    }
}