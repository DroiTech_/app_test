package com.example.app.data

object AppConst {
    const val BASE_URL: String = "https://jsonplaceholder.typicode.com"
    const val IMAGE_BASE_URL: String = "https://source.unsplash.com/collection/542909/?sig="
    const val DATABASE_NAME: String = "posts_db"
}