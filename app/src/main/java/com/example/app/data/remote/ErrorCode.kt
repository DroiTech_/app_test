package com.example.app.data.remote

object ErrorCode {
    const val NO_INTERNET = -1
    const val UNKNOWN_ERROR = -2
}